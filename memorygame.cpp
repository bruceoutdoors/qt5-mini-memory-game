#include "memorygame.h"
#include "tile.h"

#include <random>
#include <chrono>
#include <algorithm>

#include <QtWidgets>
#include <QDebug>
//#include <QtTest/QTest> // be sure you add QT += testlib in your .pro file!!

static const int SHOW_DURATION = 500;

MemoryGame::MemoryGame(QStringList &paths_list, QString &unknown_tile,
                       int column, QWidget *parent) : QWidget(parent)
{
    Tile::setUnknownTile(unknown_tile);
    QGridLayout *mainlayout = new QGridLayout;
    first_click = true;

    int r = 0;
    int c = 0;
    total_matches = 0;
    num_attempts = 0;
    matches = 0;

    //setup timers
    timer_match = new QTimer(this);
    timer_missed = new QTimer(this);
    timer_match->setSingleShot(true);
    timer_missed->setSingleShot(true);
    timer_match->setInterval(SHOW_DURATION);
    timer_missed->setInterval(SHOW_DURATION);
    connect(timer_match, SIGNAL(timeout()), this, SLOT(matchedTile()));
    connect(timer_missed, SIGNAL(timeout()), this, SLOT(missedTile()));

    // double size and duplicate all contents:
    for(QString image_path : paths_list) {
        total_matches++;
        paths_list.append(image_path);
    }
    qDebug() << "total matches to win: " << total_matches;

    // obtain a time-based seed:
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    shuffle (paths_list.begin(), paths_list.end(), std::default_random_engine(seed));

    // fill grid with tiles
    for(QString image_path : paths_list) {
        Tile *a = new Tile(image_path);
        connect(a, SIGNAL(clicked()), this, SLOT(tileClicked()));
        mainlayout->addWidget(a, r, c, 1, 1);
        mainlayout->setColumnMinimumWidth(c, a->width());
        c++;
        if(c == column) {
            c = 0;
            mainlayout->setRowMinimumHeight(r, a->height());
            r++;
        }
    }

//    mainlayout->setSizeConstraint(QLayout::SetFixedSize);
    mainlayout->setSpacing(15);

    setLayout(mainlayout);

}

void MemoryGame::tileClicked()
{
    // check if timers are running, if so turn off and
    // immediately execute function:
    if(timer_match->isActive()) {
        timer_match->stop();
        matchedTile();
    } else if(timer_missed->isActive()) {
        timer_missed->stop();
        missedTile();
    }

    // QObject::sender() returns a pointer to the object sending the signal,
    // in which we store it in clickedTile
    clicked_tile = (Tile*)sender();
    if(first_click) {
//        qDebug() << "first click.";
        stored_path = clicked_tile->getTilePath();
        clicked_tile->flipReveal();
        stored_tile = clicked_tile;
        first_click = false;
        return;
    } else if(stored_tile != clicked_tile && stored_path == clicked_tile->getTilePath()) {
        qDebug() << "match found!";
        clicked_tile->flipReveal();
        timer_match->start();
    } else {
        qDebug() << "You missed. Try again.";
        clicked_tile->flipReveal();
        timer_missed->start();
    }
    first_click = true;
    qDebug() << "number of attempts: " << ++num_attempts;

}

void MemoryGame::matchedTile()
{
    stored_tile->hide();
    clicked_tile->hide();
    qDebug() << ++matches << " matches of " << total_matches;
    if(matches == total_matches) {
        qDebug() << "O, congrats. You won.";
        QMessageBox msgBox;
        msgBox.setText(QString("Congrats. You won with %1 attempts")
                       .arg(num_attempts));
        msgBox.exec();
        this->close();
    }
}

void MemoryGame::missedTile()
{
    stored_tile->flipUnknown();
    clicked_tile->flipUnknown();
}
