#ifndef TILE_H
#define TILE_H

#include <QLabel>

class Tile : public QLabel
{
    Q_OBJECT
public:
    Tile(const QString &path_name, QWidget *parent = 0);
    QString getUnknownTile() const {return Tile::unknown;} // returns the unknown path
    QString getTilePath() const {return tile_path;}
    void static setUnknownTile(QString path_name) {unknown = path_name;}
    bool setTile(QString &path_name); // set path, returns bool whether succeed
    void flipReveal()  { setTile(tile_path); isTileRevealed = true; }
    void flipUnknown() { setTile(unknown); isTileRevealed = false; }
    bool isRevealed() { return isTileRevealed; }

signals:
    void clicked();

protected:
    void mousePressEvent ( QMouseEvent* event );

private:
    static QString unknown;
    QString tile_path;
    bool isTileRevealed;
};

#endif // TILE_H
