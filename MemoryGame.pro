# testlib needed for qWait(the pause for the tiles)
QT += widgets # testlib

QMAKE_CXXFLAGS += -std=c++11

HEADERS += \
    tile.h \
    memorygame.h

SOURCES += \
    main.cpp \
    memorygame.cpp \
    tile.cpp

RESOURCES += \
    images.qrc
