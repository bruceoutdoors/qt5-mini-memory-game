#include "memorygame.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QStringList image_paths;
    image_paths << ":/images/purple.jpg"
                << ":/images/orange.jpg"
                << ":/images/leaf.jpg"
                << ":/images/longears.jpg"
                << ":/images/blue.jpg"
                << ":/images/shortie.jpg";
    QString question_mark = ":/images/question.png";

    MemoryGame w(image_paths, question_mark, 3);

    w.show();
    
    return a.exec();
}
