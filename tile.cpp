#include "tile.h"
#include <QMouseEvent>
#include <QDebug>

QString Tile::unknown;

Tile::Tile(const QString &path_name, QWidget *parent) : QLabel(parent)
{
    tile_path = path_name;
    flipUnknown();
}

bool Tile::setTile(QString &path_name)
{
    QPixmap mypixmap;
    mypixmap.load(path_name);

    if (mypixmap.isNull()) {
        qDebug() << "Unable to load image: " << path_name;
        return false;
    }

    this->setPixmap(mypixmap);
    this->adjustSize();

    return true;
}

void Tile::mousePressEvent ( QMouseEvent * event )
{
    emit clicked();
    event->accept();
}
