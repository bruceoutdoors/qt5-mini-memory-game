#ifndef MEMORYGAME_H
#define MEMORYGAME_H

#include <QWidget>
class Tile;

class MemoryGame : public QWidget
{
    Q_OBJECT
public:
    explicit MemoryGame(QStringList &paths_list, QString &unknown_tile, int column, QWidget *parent = 0);
    
public slots:
    void tileClicked();
    void matchedTile();
    void missedTile();
    
private:
    bool first_click;
    int matches;
    int total_matches;
    int num_attempts;
    QString stored_path;
    Tile *stored_tile;
    Tile *clicked_tile;
    QTimer *timer_match;
    QTimer *timer_missed;
};

#endif // MEMORYGAME_H
